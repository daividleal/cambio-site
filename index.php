<?php 

require_once("lib/raelgc/view/Template.php");
use raelgc\view\Template;

$template = new Template("html/home.html");
$home = $template -> parse();

$template = new Template("html/project.html");
$project = $template -> parse();

$template = new Template("html/crew.html");
$crew = $template -> parse();

$template = new Template("html/media.html");
$media = $template -> parse();

$template = new Template("html/download.html");
$download = $template -> parse();

$template = new Template("html/contact.html");
$contact = $template -> parse();

$main = new Template("html/main.html");
$main->HOME = $home;
$main->PROJECT = $project;
$main->CREW = $crew;
$main->MEDIA = $media;
$main->DOWNLOAD = $download;
$main->CONTACT = $contact;
$main -> show();


?>