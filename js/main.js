function set_heights() {
	// 
	vph = $(window).height();
	vpw = $(window).width();

	nav_height = $("nav#nav").height();
	nav_div = $("div#navdiv").height();
	dif = nav_height - nav_div;
	$("div#navdiv").css({
		"margin-top" : dif/2,
		"margin-bottom" : dif/2
	});

	$("div.corpus").css({"margin-top" : nav_height});

	$("div.content").height(vph);	
	$("div#home-bg").height(vph);

}

function carrousel_next() {
	var next = $("div.corpus.active").attr("data-next");
	$("div.corpus").toggleClass("active", false);
	$(next).toggleClass("active", true);
	$("div.corpus").css("display","none");
	$(next).fadeIn();
}

function carrousel_prev() {
	var prev = $("div.corpus.active").attr("data-prev");
	$("div.corpus").toggleClass("active", false);
	$(prev).toggleClass("active", true);
	$("div.corpus").css("display","none");
	$(prev).fadeIn();
}

function scroll() {
	$(".scroll").click(function (event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
   	});
}

function highlight_cambistas() {
	$("p#c1-p").hover(function() {
		$("img#c1-img").css({
			"-webkit-filter": "grayscale(0%)",
			"filter": "grayscale(0%)"
		});
	}, function() {
		$("img#c1-img").css({
			"-webkit-filter": "grayscale(100%)",
			"filter": "grayscale(100%)"
		});
	});

	$("p#c2-p").hover(function() {
		$("img#c2-img").css({
			"-webkit-filter": "grayscale(0%)",
			"filter": "grayscale(0%)"
		});
	}, function() {
		$("img#c2-img").css({
			"-webkit-filter": "grayscale(100%)",
			"filter": "grayscale(100%)"
		});
	});
}

function mouseover_grayscale() {
	$("img.gogray").each(function() {
		$(this).hover(function() {
			$(this).css({
				"-webkit-filter": "grayscale(0%)",
				"filter": "grayscale(0%)"
			});
		}, function() {
			$(this).css({
				"-webkit-filter": "grayscale(100%)",
				"filter": "grayscale(100%)"
			});
		});
	});
}


$( document ).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
	set_heights();
	scroll();
	highlight_cambistas();
	mouseover_grayscale()	


});

$( window ).resize(function () {
	set_heights();
});

$( window ).scroll(function () {
	var divs = ["#home","#project","#crew","#media","#download","#contact"],
		div;

	// divs alvo ------------------------------------------------
	var div_project_top = $("#project").position().top;
	var div_crew_top = $("#crew").position().top;
//	var div_media_top = $("#media").position().top;
	var div_download_top = $("#download").position().top;
	var div_contact_top = $("#contact").position().top;
	//-----------------------------------------------------------

	if ($(this).scrollTop() < div_project_top)
		{ div = divs[0]; }
	else if ($(this).scrollTop() < div_crew_top && $(this).scrollTop() >= div_project_top)
		{ div = divs[1]; }
//	else if ($(this).scrollTop() < div_media_top && $(this).scrollTop() >= div_crew_top)
//		{ div = divs[2]; }
	else if ($(this).scrollTop() < div_download_top && $(this).scrollTop() >= div_crew_top)
		{ div = divs[2]; }
//	else if ($(this).scrollTop() < div_download_top && $(this).scrollTop() >= div_media_top)
//		{ div = divs[3]; }
	else if ($(this).scrollTop() < div_contact_top 	&& $(this).scrollTop() >= div_download_top)
		{ div = divs[4]; }
	else if ($(this).scrollTop() >= div_contact_top)
		{ div = divs[5]; }

	$("nav#nav a").toggleClass("active",false);
	$("nav#nav a").each(function () {
		if ($(this).attr("href") == div) {
			href = $(this).attr("href");
			$(this).toggleClass("active", true);
		}
		
	});
});